#!/bin/bash

# LAB_LOG: 
# AUTHOR: Michael M. Folkerts
#
# Copy and paste what you need into the command line
# For initial setup, you can just run this script!!

#install common libraries:
#allow comment lines in repository file
grep -v '#' repositories.txt | xargs sudo apt-get -y install
#For development libraries:
#grep -v '#' repositories-dev.txt | xargs sudo apt-get -y install


#cd lib/
#Install dicom-rt library (takes a long time):
# maybe we can get away with just finding the lib binaries and copying them over?
#tar -xf dcmrt_20110221.tar.gz
#cd dcmrt_20110221
#./configure --enable-std-includes
#make all
#for some reason do both to be sure (it's fast)
#sudo make install
#sudo make install-lib
#back to root directory
#cd ../../



# Thrift
# http://wiki.apache.org/thrift/ThriftInstallation
cd lib/
tar -xf thrift-0.8.0.tar.gz
cd thrift-0.8.0
#By default, Erlang library is built, but may have a bug during compilation
./configure --with-erlang=no
make
sudo make install

# needed to do this to make thrift work on head node
# http://mail-archives.apache.org/mod_mbox/incubator-thrift-user/201004.mbox/%3Cu2w2739afa1004030005uaad3c8a8k1c4f61127b2c3116@mail.gmail.com%3E

cd lib/py
sudo python setup.py install

cd ../../../../
