#!/bin/bash

# LAB_LOG: 
# AUTHOR: Michael M. Folkerts
#
# Copy and paste what you need into the command line
# For initial setup, you can just run this script!!

#install cuda4.0
cd lib/
mkdir cuda4.0
cd cuda4.0
wget http://developer.download.nvidia.com/compute/cuda/4_0/drivers/devdriver_4.0_linux_64_270.41.19.run \
http://developer.download.nvidia.com/compute/cuda/4_0/toolkit/cudatoolkit_4.0.17_linux_64_ubuntu10.10.run \
http://developer.download.nvidia.com/compute/cuda/4_0/sdk/gpucomputingsdk_4.0.17_linux.run
# allow execution
chmod a+x *
# put copy of SDK installer in public place
sudo mkdir /usr/local/cudaSDK
sudo cp gpucomputingsdk_4.0.17_linux.run /usr/local/cudaSDK/
sudo mkdir /usr/local/cudaDRIVER
sudo cp devdriver_4.0_linux_64_270.41.19.run /user/local/cudaDRIVER/
# install needed packages
sudo apt-get -y install binutils gcc
# we also need to assure we have the source code for the currently active linux kernel
sudo apt-get -y install linux-headers-`uname -r`
# Install CUDA developer dirvers (Accept lic. agreement, suppress Questions, no UI, install OPENGL headers, no DISTRO scrips)
sudo ./devdriver_4.0_linux_64_270.41.19.run -aq --ui=none --opengl-headers --no-distro-scripts
# Install the CUDA toolkit
sudo ./cudatoolkit_4.0.17_linux_64_ubuntu10.10.run
# setup profile and cuda startup scrips
cd ../
sudo cp cuda_profile.sh /etc/profile.d/
# make sure script is executable
chmod a+x startCuda.sh
sudo cp startCuda.sh /etc/init.d/
sudo update-rc.d startCuda.sh defaults
# start up cuda
sudo /etc/init.d/startCuda.sh
cd ../
#TODO: insert a script to re-install kernel modules on linux kernel upgrade: /etc/kernel/postinst.d/
cp cudaDriverInstall.sh /etc/kernal/postinst.d/


#install common libraries:
#allow comment lines in repository file
grep -v '#' repositories.txt | xargs sudo apt-get -y install

# For development libraries:
#grep -v '#' repositories-dev.txt | xargs sudo apt-get -y install

# For cuda SDK:
#sudo apt-get -y install freeglut3-dev build-essential libx11-dev libxmu-dev libxi-dev libgl1-mesa-glx libglu1-mesa libglu1-mesa-dev
#/usr/local/cudaSDK/gpucomputingsdk_4.0.17_linux.run

cd lib/
#Install dicom-rt library (takes a long time):
# maybe we can get away with just finding the lib binaries and copying them over?
tar -xf dcmrt_20110221.tar.gz
cd dcmrt_20110221
./configure --enable-std-includes
make all
#for some reason do both to be sure (it's fast)
sudo make install
sudo make install-lib
#back to root directory
cd ../../



# Thrift
# http://wiki.apache.org/thrift/ThriftInstallation
cd lib/
tar -xf thrift-0.8.0.tar.gz
cd thrift-0.8.0
#By default, Erlang library is built, but may have a bug during compilation
./configure --with-erlang=no
make
sudo make install
cd ../../
