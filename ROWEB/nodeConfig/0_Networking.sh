#!/bin/bash

# run as root
# There may be a way to do this without restarting.

# copy file to generate new rules
# cp /lib/udev/rules.d/75-persistent-net-generator.rules /etc/udev/rules.d/
# remove old rules file
rm /etc/udev/rules.d/70-persistent-net.rules

# copy over hosts list
echo "Copying hosts list..."
cp etc/hosts /etc/hosts

echo "Copying environment..."
cp etc/environment /etc/environment

# set hostname
echo "Setting hostname... Please enter node ID number (01-99):"
read nodeNum
echo "titan-gpu$nodeNum" > /etc/hostname

# dns settin in /etc/network/interfaces
# set dns
#echo "Setting nameservers..."
#cp etc/resolv.conf /etc/resolv.conf
#newer network config because of dynamic resolv.conf
#cat /etc/resolv.conf >> /etc/resolvconf/resolv.conf.d/head

# copy over basic networking file
cp etc/network/interfaces /etc/network/interfaces
echo -e  "\taddress 10.90.91.1$nodeNum" >> /etc/network/interfaces
echo "Basic network info saved in /etc/network/interfaces, link aggregation not configured."
echo "Press 'Enter' to restart. After restart, run ./Setup.sh as root."
read enter

#copy for apt (proxy)
cp etc/apt/apt.conf /etc/apt/apt.conf

# restart to apply changes
shutdown -r 0

