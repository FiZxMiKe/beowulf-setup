#!/bin/bash

# Author: Michael M. Folkerts (bit.ly/folkerts) mfolkerts@physics.ucsd.edu
# GACCIP Project, CART, Dept. of Radiation Medicine and Applied Sciences, UCSD


# run as root!
# this script sets up a gpu-data node

# clean up network setup
echo "Cleaning up network auto-configuration..."
# remove rules generator file
rm /etc/udev/rules.d/75-persistent-net-generator.rules

# add admin user (manually set password, sorry)
#echo "Creating local admin account (pico) please enter admin password..."
#adduser pico
#adduser pico admin

# copy over pubkey
#echo "Setting up pubkey access from head node..."
#mkdir /home/gaccip/.ssh
#cp ssh/gaccip-head.pubkey /home/gaccip/.ssh/authorized_keys
#chown --recursive gaccip:gaccip /home/gaccip/.ssh

#echo "placing checout script in gaccip home directory..."
#cp sh/nodeCheckout.sh /home/gaccip/

# run apt update
echo "Updating package list and upgrading packages..."
apt-get update
apt-get -y upgrade

# install needed packages
echo "Installing needed packages from 'packages.txt' ..."
xargs apt-get -y install < packages.txt

#probe first node for gluster

echo "Stting up likewise open (with mfolke login)"
sudo domainjoin-cli join utmroc.swmed.org mfolke
sudo lwconfig AssumeDefaultDomain true
sudo lwconfig --file lw.conf
sudo lwconfig RequireMembershipOf "UTMROC\\pico_users"
#sudo lwconfig RequireMembershipOf "UTMROC\\physics^res^coders"
