
### install cuda 5.0 on slave nodes

## download and install
echo moving to CUDA/cuda-5.5 directory...
cd CUDA/cuda-5.5

echo Extracting installer files...
./cuda_5.5.* -extract=`pwd`

#make installers executable
#chmod a+x *

# put copy of installers in public place
sudo mkdir -p /usr/local/cuda-toolkit
sudo cp cuda-linux64-rel* /usr/local/cuda-toolkit/

sudo mkdir -p /usr/local/cuda-driver
sudo cp NVIDIA-Linux-x86_64* /usr/local/cuda-driver/

sudo mkdir -p /usr/local/cuda-samples
sudo cp cuda-samples-linux* /usr/local/cuda-toolkit/

# install needed packages
sudo apt-get -y install binutils gcc

# we also need to assure we have the source code for the currently active linux kernel (really?)
sudo apt-get -y install linux-headers-`uname -r`

# Install the CUDA toolkit
sudo ./cuda-linux64-rel* -noprompt

# Install (but not build) the CUDA samples (got error here looking for proper path '-noprompt broken' ?)
sudo ./cuda-samples-linux*

#create symlink to current cuda versionls 
#done automatically?

# update LD.SO
sudo cp ../../etc/ld.so.conf.d/cuda.conf /etc/ld.so.conf.d/cuda.conf
sudo ldconfig -v

#handled in /etc/environment now
# copy global profile to add cuda to path and LD library path
#sudo sh -c "cat /etc/profile-cuda.txt >> /etc/profile"
