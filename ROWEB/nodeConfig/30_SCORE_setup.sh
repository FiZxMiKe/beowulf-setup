#install UI and xrdp
sudo apt-get install -y --no-install-recommends xubuntu-desktop xubuntu-icon-theme
sudo apt-get remove -y nvidia-common
sudo apt-get install -y xrdp qtcreator leafpad

#Install dicom-rt library (takes a long time):
# maybe we can get away with just finding the lib binaries and copying them over?
cd lib
tar -xf dcmrt_20110221.tar.gz
cd dcmrt_20110221
./configure --enable-std-includes
make all
#for some reason do both to be sure (it's fast)
sudo make install
sudo make install-lib
#back to root directory
cd ../../

echo "comment out the super-tab portion of the next file ..."
read x
sudo nano /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml

echo "comment out the browser properties in the next file ..."
read x
sudo nano /etc/xdg/xfce4/panel/default.xml

echo "now in /etc/xdg/autostart/user-dirs.conf, change setting to false ..."
read x
sudo nano /etc/xdg/user-dirs.conf

sudo cp etc/xrdp/startwm.sh /etc/xrdp/startwm.sh
sudo /etc/init.d/xrdp restart

echo "you are now ready for xrdp logins!"