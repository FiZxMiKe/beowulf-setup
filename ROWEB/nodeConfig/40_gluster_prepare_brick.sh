#prepare brick
echo "FDISK will show you the drives, please make note of the dev ids you wish to use"
read x

sudo fdisk -l

echo "enter device prefix /dev/sd?"
read LETTER

echo "you must make fresh partition table (n, defaults, w)"
read x
sudo fdisk /dev/sd${LETTER}
sudo mkfs.xfs -i size=512 /dev/sd${LETTER}1
sudo mkdir -p /data/sd${LETTER}
sudo mount /dev/sd${LETTER}1 /data/sd${LETTER}
sudo mkdir /data/sd${LETTER}/brick

#now we grab the UUID
UUID_TEMP=`sudo blkid /dev/sd${LETTER}1 -o udev | grep UUID= | cut -c12-`
#and put it in FS TAB
sudo sh -c  "echo 'UUID=${UUID_TEMP} /data/sd${LETTER} xfs defaults 0 0'  >> /etc/fstab"
