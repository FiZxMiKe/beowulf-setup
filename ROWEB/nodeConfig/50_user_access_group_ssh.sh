echo "creating node_users group"
addgroup node_users

echo "adding restrictions to /etc/ssh/sshd_config"

echo "## Allow only group access" >> /etc/ssh/sshd_config
echo "#ADMINS" >> /etc/ssh/sshd_config
echo "AllowGroups sudo" >> /etc/ssh/sshd_config
echo "#GPU02 USERS" >> /etc/ssh/sshd_config
echo "AllowGroups node_users" >> /etc/ssh/sshd_config

sudo service ssh restart 

echo "please add the proper people to the 'node_users' group to enable ssh (and xrdp) access"
