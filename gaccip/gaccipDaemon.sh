#!/bin/bash

#launch test server as gaccip user:

#echo $0
#echo $1

cd /home/gaccip/bin/

if [ "$1" == "start" ]
then
	echo "launching server"
	export LD_LIBRARY_PATH=/usr/local/cuda/lib64:/usr/local/cuda/lib
	su gaccip -c "./ControlServerQt config &> ../logFile.txt &"
fi

if [ "$1" == "stop" ]
then
	echo "killing server"
	pid=`ps -A | grep ControlServerQt | awk '{print $1}'`
	kill -s TERM $pid
fi
