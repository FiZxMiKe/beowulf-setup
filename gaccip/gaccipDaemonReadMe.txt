use the script located in ../setup/headConfig/makeStartupScript.sh to install gaccipDaemon.sh

you can use the following to stop and start the server (but it will be launched automatically at boot)
sudo /etc/init.d/gaccipDaemon.sh stop
sudo /etc/init.d/gaccipDaemon.sh start
