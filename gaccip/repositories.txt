# This comment line gets filtered by: grep -v '#'

# Misc Libs
libmagick++3
#libargtable2

# Python Libs
python-argparse
python-numpy
python-matplotlib

# Qt Libs
libqt4-core
libqt4-gui
libqt4-xml
libqt4-network

# Required to build thrift on Ubuntu
## http://wiki.apache.org/thrift/GettingDebianPackages?action=show&redirect=GettingUbuntuPackages 
libboost-dev
libboost-test-dev
libboost-program-options-dev
libevent-dev
automake
libtool
flex
bison
pkg-config
g++
libssl-dev
