#! /bin/bash

#first setup etc (apt-Proxy, environment-proxy,hostname, hosts, rinetd port forwarding for ssh and rdp)
cp -rf etc/* /etc/

#then copy nat script (router)
chmod a+x routerNAT.sh
cp routerNAT.sh /etc/init.d/
ln -s /etc/init.d/routerNAT.sh /etc/rc2.d/S95masquradescript
update-rc.d routerNAT.sh defaults
/etc/init.d/networking restart
/etc/init.d/routerNAT.sh

apt-get update
apt-get upgrade

apt-get install rinetd
cp etc/rinetd.conf /etc/rinetd.conf
/etc/init.d/rinetd restart

