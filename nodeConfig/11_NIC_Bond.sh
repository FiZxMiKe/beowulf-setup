#!/bin/bash

# Author: Michael M. Folkerts (bit.ly/folkerts) mfolkerts@physics.ucsd.edu
# GACCIP Project, CART, Dept. of Radiation Medicine and Applied Sciences, UCSD


# run as root!
# this script sets up NIC bonding

echo "setting up round-robin bonding (requires ifenslave)"

echo "please enter the node number (ex: 01 or 12):"
read nodeNum

echo "bringing down any current network connections"
ifdown eth0
ifdown eth1

#create backup of interface file
mv /etc/network/interfaces /etc/network/interfaces.eth0
cp etc/network/interfaces.bond-rr /etc/network/interfaces
echo "address 10.90.91.1$nodeNum" >> /etc/network/interfaces

#echo "bringing up bond0"
#ifup bond0

echo "restarting"
reboot
