### pickup here on restart - desktop users!
cd CUDA/cuda-5.5

# Install CUDA developer dirvers (Accept lic. agreement, suppress Questions, no UI, disable faux nvidia driver, no distro scripts, dynamic kernel module source rebuild on update)

#first time
sudo ./NVIDIA-Linux-x86_64* -a --ui=none --disable-nouveau --no-distro-scripts --no-opengl-files --dkms

sudo ./NVIDIA-Linux-x86_64* -a --ui=none --disable-nouveau --no-distro-scripts --no-opengl-files --dkms --update

##For Desktop
#this needs to be run after the first run of the above installer:
#sudo update-initramfs -u
#then restart
#sudo shutdown -r 0

## setup cuda startup script

# make sure script is executable
chmod a+x ../../etc/init.d/startCuda.sh

# make it a startup script
sudo cp ../../etc/init.d/startCuda.sh /etc/init.d/
sudo update-rc.d startCuda.sh defaults

# start up cuda
sudo /etc/init.d/startCuda.sh
