
#this makes only one login option
sudo mv /etc/xrdp/xrdp.ini /etc/xrdp/xrdp.ini.old
sudo cp etc/xrdp/xrdp.ini /etc/xrdp/

#this makes only users in group "node_users" allowed to access, and a maximum of 1 session
sudo mv /etc/xrdp/sesman.ini /etc/xrdp/sesman.ini.old
sudo cp etc/xrdp/sesman.ini /etc/xrdp/

