#!/bin/bash

#re-install cuda drivers (non-interactive)
/usr/local/cudaDRIVER/devdriver_4.0_linux_64_270.41.19.run -aq --ui=none --opengl-headers --no-distro-scripts

#re-launch cuda init script
/etc/init.d/startCuda.sh
