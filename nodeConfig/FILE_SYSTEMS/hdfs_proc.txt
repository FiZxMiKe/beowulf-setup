## LIFTED FROM (credit):
## http://www.michael-noll.com/tutorials/running-hadoop-on-ubuntu-linux-single-node-cluster/

# Add the Ferramosca Roberto's repository to your apt repositories
# See https://launchpad.net/~ferramroberto/
#
sudo apt-get install python-software-properties
sudo add-apt-repository ppa:ferramroberto/java

# Update the source list
sudo apt-get update

# Install Sun Java 6 JDK
sudo apt-get install sun-java6-jdk

# Select Sun's Java as the default on your machine.
# See 'sudo update-alternatives --config java' for more information.
#
sudo update-java-alternatives -s java-6-sun

#verify install
java -version

#add hadoop system user and group
sudo addgroup hadoop
sudo adduser --ingroup hadoop hduser

#add gacip key to hduser
ssh-copy-id hduser@localhost

#add hduser keys to localhost
su - hduser
ssh-keygen -P ""
ssh-copy-id localhost
