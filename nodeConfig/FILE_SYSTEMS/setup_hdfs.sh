#!/bin/sh

CURRENT_DIR=/home/mmlee/gaccip/setup/nodeConfig
HOME=~
HADOOP_DIR=$HOME/hadoop-0.20.203.0

sudo apt-get update
sudo apt-get install -y ssh rsync
sudo apt-get install -y default-jdk

# download hadoop binary
cd $HOME
wget http://mirrors.sonic.net/apache//hadoop/common/stable/hadoop-0.20.203.0rc1.tar.gz
tar xvzf hadoop-0.20.203.0rc1.tar.gz 

cp $CURRENT_DIR/hdfs_conf/hadoop-env.sh $HADOOP_DIR/conf
cp $CURRENT_DIR/hdfs_conf/hdfs-site.xml $HADOOP_DIR/conf

# ssh-key generation (commented out)
# ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa # generating a new public-key

# copy public-key from repo
cp $CURRENT_DIR/hdfs_conf/id_rsa ~/.ssh/id_rsa
cp $CURRENT_DIR/hdfs_conf/id_rsa.pub ~/.ssh/id_rsa.pub
cat ~/.ssh/id_rsa.pub > ~/.ssh/authorized_keys

